
<div class="slider slider--big">
  <div class="slider-inner js--slider slider--dots">
    <!-- <img src="assets/images/slider/anantara-kihavah-first-class-holiday.jpg" alt="">
    <img src="assets/images/slider/anantara-kihavah-maldives-island-resort.jpg" alt="">
    <img src="assets/images/slider/anantara-kihavah-maldives-overwater-spa.jpg" alt="">
    <img src="assets/images/slider/anantara-kihavah-maldives-villa-walkway.jpg" alt=""> -->
    <div class="slider-item" style="background-image:url('assets/images/slider/anantara-kihavah-first-class-holiday.jpg')"></div>
    <div class="slider-item" style="background-image:url('assets/images/slider/anantara-kihavah-maldives-island-resort.jpg')"></div>
    <div class="slider-item" style="background-image:url('assets/images/slider/anantara-kihavah-maldives-overwater-spa.jpg')"></div>
    <div class="slider-item" style="background-image:url('assets/images/slider/anantara-kihavah-maldives-villa-walkway.jpg')"></div>
  </div>

  <div class="slider-container">
    <div class="container--xl">
      <p class="text--meta">Destinationer / Det indiske ocean</p>
      <h3 class="color--main font--italic">Rejser til 
        <br>
        Maldiverne
      </h3>
      <img class="image--contain" src="assets/images/slider-graphic.png" alt="">
      <div class="js--slider-actions slider-actions"></div>
    </div>
  </div>
</div>
<!-- Slider big end. -->

<!-- Page nav -->
<div class="toggle toggle--visibility toggle--sm">
  <input type="checkbox" id="navPage" class="toggle-checkbox">
  <label class="nav-pageToggle toggle-label" for="navPage">Menu <i class="icon--arrowDownDark ml--1"></i></label>
  <div class="nav-page toggle-container">
    <div class="container--xl--">
      <nav class="nav nav--page">
        <div class="nav-list">
          <div class="nav-item nav-item--active"><a href="#section--2" class="nav-link">Atoller</a></div>
          <div class="nav-item"><a href="#section--3" class="nav-link">Aktiviteter</a></div>
          <div class="nav-item"><a href="#section--4" class="nav-link">Klima Og Vejr</a></div>
          <div class="nav-item"><a href="#section--5" class="nav-link">Fakta Om Tanzania</a></div>
          <div class="nav-item"><a href="#section--6" class="nav-link">Hotller Pa Maldiverne</a></div>
        </div>
      </nav>
      <a href="#" class="button button--big button--navPage">Kombinationsrejser</a>
      <a href="#" class="button button--big button--navPage--secondary">alle rejser til maldiverne</a>
    </div>
  </div>
</div>
<!-- Page nav end -->

<section class="section--wide section--hasTitle bg-variant-1--1" id="section--1">
  <div class="section-title">
    <div class="container--md text--center text--md--left">
      Om Maldiverne
    </div>
  </div>
  <div class="container--md">
    <div class="text-widget grid">
      <div class="col--12 col--md--7 mb--3 mb--md--0">

        <!-- Slider small -->
        <div class="slider slider--small">
          <div class="slider-inner js--slider">
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
          </div>
          <div class="slider-expand">Store fotos</div>
          <div class="slider-container">
            <div class="js--slider-actions slider-actions">
            </div>
          </div>
        </div>
        <!-- Slider small end. -->

        <img class="image--contain image--right image--map" src="assets/images/text-map-2.png" alt="">

        <p class="p--offset--left p--icon--plane styled">Rejsen til Maldiverne er typisk med afrejse fra Kastrup, Billund eller Hamborg med mellemlanding og flyskift i en europæisk storby, Qatar eller Dubai afhængig af flyselskabet. Flyvetiden er sammenlagt fra 11 timer afhængig af ruten. Den samlede rejsetid afhænger derfor af, hvor lang tid man har til at skifte fly undervejs. Til Maldiverne benytter vi oftest flyselskaberne Emirates, Qatar Airways, Etihad, Condor, Swiss/Edelweiss og Austrian.</p>

        <p class="p--offset--left styled">Transport videre til hotellet er altid inkluderet i vores rejser ved rejser til Maldiverne med Blixen Tours har vi altid inkluderet transport fra lufthavn i Malé til hotellet tur/retur. Enten med båd eller vandflyver og til de sydligst liggende øer indenrigsfly og båd.</p>
      </div>
      <div class="col--12 col--md--5">
        <p class="text--leader">
          Når du nærmer dig Maldiverne fra luften, fornemmer du at være på vej til noget særligt. Så langt øjet rækker ses små koraløer som perler i det azurblå hav. Øerne åbenbarer sig som et tropeparadis med sol, vand og varme. Kridhvide strande med blidt svajende kokospalmer. Fugle i fantastiske farver glider henover det klareste hav, du kan forestille dig.
        </p>
        <p>
          Tag på en eksotisk rejse til Maldiverne. Fordriv ferie tiden med dykning og surfing eller sejl rundt mellem de godt 1.200 småøer, hvor nogle af verdens smukkeste og mest farverige koralrev og krystalklare laguner findes. Rejs på eventyr og oplev det fantastiske fugleliv og smag på mangoer, appelsiner og ananas.
        </p>
        <p>
          Bo i lækre bungalows langs strandkanten, eller nyd livet på de mest fantastiske hoteller med værelser, der står på pæle i vandet. Disse ”water-villas” er en helt unik og fantastisk oplevelse som kendetegner rejser til Maldiverne. Mange vælger at tage på en romantisk bryllupsrejse til dette ø-paradis, og vi forstår godt hvorfor.
        </p>
        <p>
          Maldiverne er et sandt paradis både over og under vandet. En betagende og farverig verden dukker frem så snart du kommer under havoverfladen. Elsker du derfor at dykke eller snorkle er Maldiverne det helt rigtige valg.
        </p>
        <p>
          Hold ferie på Maldiverne året rundt. Maldiverne byder jer nemlig varmt velkommen med sol og kridhvide bountystrande og temperaturer året rundt på omkring 30 grader.
        </p>
        <p>
          En rejse til Maldiverne byder på alt hvad du drømmer om, hvis du er til ultimativ luksus på en lille bounty-ø. Til trods for at øerne er små, er der et utroligt højt serviceniveau samt udbyd af aktiviteter. Maldiverne rejser kan derfor byde på langt mere end bare en ferie på en kridhvid sandstrand. Der kan være stor forskel på øerne på Maldiverne, så forhør dig derfor gerne hos os om, hvilken ø der passer bedst til jeres ønsker. 
        </p>
        <p>
          Når du rejser til Maldiverne med Blixen Tours er det muligt at flyve fra de større danske lufthavne - Kastrup og Billund - samt Hamborg. Der kan i visse perioder være en prismæssig fordel i at rejse til Maldiverne fra Tyskland. Skriv eller ring og hør nærmere om dine muligheder.
        </p>
        <a href="#" class="read-more">Fuld beskrivelse<span class="fa fa-angle-down"></span></a>
      </div>
    </div>
  </div>
</section> <!-- section 1 -->
<section class="section--wide section--hasTitle bg-variant-4--1" id="section--5">
  <div class="section-title">
    <div class="container--md text--center text--md--left">
      <strong>Atoller</strong> <br> maldiverne
    </div>
  </div>

  <div class="container--md">
    <div class="text-widget grid">
      <div class="col--12 col--md--7 mb--3 mb--md--0">

        <!-- Slider small -->
        <div class="slider slider--small mb--4">
          <div class="slider-inner js--slider">
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
          </div>
          <div class="slider-expand">Store fotos</div>
          <div class="slider-container">
            <div class="js--slider-actions slider-actions">
            </div>
          </div>
        </div>
        <!-- Slider small end. -->

      </div>
      <div class="col--12 col--md--5">
        <p class="text--leader">
          Maldiverne består af cirka 1.200 øer, der er samlet i 26 større klynger af øer - nærmere betegnet atoller - som er opstået omkring vulkaner og senere er sunket i havet. Kun cirka 200 af øerne er beboede.
        </p>
        <p>
          Øerne er tilsammen på størrelse med halvdelen af Bornholm. Den sydligste del af den 750 kilometer lange øgruppe krydser ækvator og højeste punkt er blot 1,5 meter over havets overflade. Mange atoller ses kun som turkise aftegninger under havoverfladen. En naturoplevelse i sig selv at betragte fra en vandflyver og en ekstra dimension til rejsen til Maldiverne.
        </p>
        <p>
          Hver atolgruppe på Maldiverne har et navn og sin egen administrative enhed, nærmest at sammenligne med amter.
        </p>


      </div>
    </div>
  </div>
</section> <!-- section 5 -->
<section class="section--wide bg-variant-4--1 py--0" id="section--4">
  <div class="map">
    <div class="js--map map-widget w--100" id="map"></div>
    <div class="map-legend hidden">
      <p class=""><strong>områder og parker</strong> <br> Botswana luksus safarirejse</p>
      <div class="list list--bordered">
        
        <div class="list-title">Områder og parker</div>
        <div class="list-item"><i class="icon--number mr--2">1</i>Sanctuary Chief´s Camp</div>
        <div class="list-item"><i class="icon--number mr--2">2</i>Sanctuary Chief´s Camp</div>
        <div class="list-item"><i class="icon--number mr--2">3</i>Sanctuary Chief´s Camp</div>
        <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>
        <div class="list-item"><i class="icon--number mr--2">4</i>Sanctuary Chief´s Camp</div>

      </div>
    </div>
  </div>
</section> <!-- section 4 -->
<section class="section--wide section--hasTitle bg-variant-4--1 hidden" id="section--3">
  <div class="section-title">
    <div class="container--md text--center text--md--left">
      områder og parker <br> tanzania
      <a href="" class="float--right font--italic text--underlined text--capitalize text--strong">Fuld beskrivelse <span class="fa fa-angle-down"></span></a>
    </div>
  </div>
  <div class="container--md">
    <div class="accordion">
      <div class="accordion-header">
        <i class="icon--number mr--2">1</i>
        Sanctuary Chief´s Camp
      </div>
      <h3 class="accordion-header">
        <i class="icon--number mr--2">2</i>
        Sanctuary Chief´s Camp
      </h3>
      <div class="accordion-content">
        <div class="text-widget grid mb--6">
          <div class="col--12 col--md--7 mb--3 mb--md--0">
            
            <img class="image--contain mb--1" src="assets/images/accordion-image.jpg">
            <p class="text--smaller">
              Serengeti Nationalpark
            </p>

          </div>
          <div class="col--12 col--md--5">
            <p>Navnet Serengeti kommer fra ”siringet”, der på Masai-folkets sprog betyder “endeløse sletter”, og med rette. Serengeti er et vildtområde på 14.763 km², hvor godt to tredjedele er sletter. I den nordligere del er landskabet meget kuperet, og i den centrale del er der tæt akacievegetation. </p>
            <p>I Serengeti finder man en bestand af storvildt, som ikke findes mage til andre steder på jorden. Mod nord grænser parken op til Masai Mara-reservatet i Kenya. De to parker danner et økosystem, hvor omkring to millioner dyr er på konstant vandring (”migration” på engelsk) efter nye græsgange – størstedelen er gnuer, zebraer og gazeller. Rovdyrene følger flokken som en skygge, altid på jagt efter næste måltid. </p>
            <p>I Serengeti er alle vildtarter repræsenteret, og safarien byder altid på spændende oplevelser. Perioden fra omkring december til juni er de bedste måneder at opleve migration.</p>
          </div>
        </div>
      </div> <!-- accordion-content -->

      <h3 class="accordion-header">
        <i class="icon--number mr--2">3</i>
        Sanctuary Chief´s Camp
      </h3>
      <div class="accordion-content">
        <div class="col--12 col--md--7 mb--3 mb--md--0">
          <div class="col--12 col--md--6">
            
            <img class="image--contain mb--1" src="assets/images/accordion-image.jpg">
            <p class="text--smaller">
              Serengeti Nationalpark
            </p>

          </div>
          <div class="col--12 col--md--5">
            <p>Navnet Serengeti kommer fra ”siringet”, der på Masai-folkets sprog betyder “endeløse sletter”, og med rette. Serengeti er et vildtområde på 14.763 km², hvor godt to tredjedele er sletter. I den nordligere del er landskabet meget kuperet, og i den centrale del er der tæt akacievegetation. </p>
            <p>I Serengeti finder man en bestand af storvildt, som ikke findes mage til andre steder på jorden. Mod nord grænser parken op til Masai Mara-reservatet i Kenya. De to parker danner et økosystem, hvor omkring to millioner dyr er på konstant vandring (”migration” på engelsk) efter nye græsgange – størstedelen er gnuer, zebraer og gazeller. Rovdyrene følger flokken som en skygge, altid på jagt efter næste måltid. </p>
            <p>I Serengeti er alle vildtarter repræsenteret, og safarien byder altid på spændende oplevelser. Perioden fra omkring december til juni er de bedste måneder at opleve migration.</p>
          </div>
        </div>
      </div> <!-- accordion-content -->

      <h3 class="accordion-header">
        <i class="icon--number mr--2">4</i>
        Sanctuary Chief´s Camp
      </h3>
      <div class="accordion-content">
        <div class="text-widget grid mb--6">
          <div class="col--12 col--md--7 mb--3 mb--md--0">
            
            <img class="image--contain mb--1" src="assets/images/accordion-image.jpg">
            <p class="text--smaller">
              Serengeti Nationalpark
            </p>

          </div>
          <div class="col--12 col--md--5">
            <p>Navnet Serengeti kommer fra ”siringet”, der på Masai-folkets sprog betyder “endeløse sletter”, og med rette. Serengeti er et vildtområde på 14.763 km², hvor godt to tredjedele er sletter. I den nordligere del er landskabet meget kuperet, og i den centrale del er der tæt akacievegetation. </p>
            <p>I Serengeti finder man en bestand af storvildt, som ikke findes mage til andre steder på jorden. Mod nord grænser parken op til Masai Mara-reservatet i Kenya. De to parker danner et økosystem, hvor omkring to millioner dyr er på konstant vandring (”migration” på engelsk) efter nye græsgange – størstedelen er gnuer, zebraer og gazeller. Rovdyrene følger flokken som en skygge, altid på jagt efter næste måltid. </p>
            <p>I Serengeti er alle vildtarter repræsenteret, og safarien byder altid på spændende oplevelser. Perioden fra omkring december til juni er de bedste måneder at opleve migration.</p>
          </div>
        </div>
      </div> <!-- accordion-content -->

      <h3 class="accordion-header">
        <i class="icon--number mr--2">5</i>
        Sanctuary Chief´s Camp
      </h3>
      <div class="accordion-content">
        <div class="text-widget grid mb--6">
          <div class="col--12 col--md--7 mb--3 mb--md--0">
            
            <img class="image--contain mb--1" src="assets/images/accordion-image.jpg">
            <p class="text--smaller">
              Serengeti Nationalpark
            </p>

          </div>
          <div class="col--12 col--md--5">
            <p>Navnet Serengeti kommer fra ”siringet”, der på Masai-folkets sprog betyder “endeløse sletter”, og med rette. Serengeti er et vildtområde på 14.763 km², hvor godt to tredjedele er sletter. I den nordligere del er landskabet meget kuperet, og i den centrale del er der tæt akacievegetation. </p>
            <p>I Serengeti finder man en bestand af storvildt, som ikke findes mage til andre steder på jorden. Mod nord grænser parken op til Masai Mara-reservatet i Kenya. De to parker danner et økosystem, hvor omkring to millioner dyr er på konstant vandring (”migration” på engelsk) efter nye græsgange – størstedelen er gnuer, zebraer og gazeller. Rovdyrene følger flokken som en skygge, altid på jagt efter næste måltid. </p>
            <p>I Serengeti er alle vildtarter repræsenteret, og safarien byder altid på spændende oplevelser. Perioden fra omkring december til juni er de bedste måneder at opleve migration.</p>
          </div>
        </div>
      </div> <!-- accordion-content -->

      <h3 class="accordion-header">
        <i class="icon--number mr--2">6</i>
        Sanctuary Chief´s Camp
      </h3>
      <div class="accordion-content">
        <div class="text-widget grid mb--6">
          <div class="col--12 col--md--7 mb--3 mb--md--0">
            
            <img class="image--contain mb--1" src="assets/images/accordion-image.jpg">
            <p class="text--smaller">
              Serengeti Nationalpark
            </p>

          </div>
          <div class="col--12 col--md--5">
            <p>Navnet Serengeti kommer fra ”siringet”, der på Masai-folkets sprog betyder “endeløse sletter”, og med rette. Serengeti er et vildtområde på 14.763 km², hvor godt to tredjedele er sletter. I den nordligere del er landskabet meget kuperet, og i den centrale del er der tæt akacievegetation. </p>
            <p>I Serengeti finder man en bestand af storvildt, som ikke findes mage til andre steder på jorden. Mod nord grænser parken op til Masai Mara-reservatet i Kenya. De to parker danner et økosystem, hvor omkring to millioner dyr er på konstant vandring (”migration” på engelsk) efter nye græsgange – størstedelen er gnuer, zebraer og gazeller. Rovdyrene følger flokken som en skygge, altid på jagt efter næste måltid. </p>
            <p>I Serengeti er alle vildtarter repræsenteret, og safarien byder altid på spændende oplevelser. Perioden fra omkring december til juni er de bedste måneder at opleve migration.</p>
          </div>
        </div>
      </div> <!-- accordion-content -->
      
    </div>
  </div>
</section> <!-- section 3 -->
<section class="section--wide section--hasTitle bg-variant-1--1" id="section--6">
  <div class="section-title">
    <div class="container--md text--center text--md--left">
      Klima og vejr <br> <span>Maldiverne</span>
    </div>
  </div>

  <div class="container--md">
    <div class="text-widget grid mb--5 mb--md--7">
      <div class="col--12 col--md--7 mb--3 mb--md--0">

        <h4>
          Vi bliver ofte spurgt om ’hvornår er det bedste tidspunkt at rejse til Maldiverne?’ Du kan besøge Maldiverne året rundt.
        </h4>

      </div>
      <div class="col--12 col--md--5">
        <p class="">
          Maldiverne er en særdeles attraktiv rejsedestination året rundt. På grund af øgruppens beliggenhed uden for cyklonbæltet og tæt ved ækvator skinner solen de fleste dage, og dagtemperaturen ligger omkring 30 °C uanset årstiden. Nattemperaturen omkring 25-26 30 °C. Temperaturen varierer meget lidt, og varmen er behagelig - og så er havvandet altid lunt; omkring 28 grader året rundt.
        </p>
        <p>
          <a href="#" class="read-more">Fuld beskrivelse<span class="fa fa-angle-down"></span></a>
        </p>
      </div>
    </div>

    <div class="temperature-widgetWrap">
      <h5 class="temperature-widgetTitle">Lufttemperatur</h5>
      <div class="temperature-widget">
        <div class="temperature-month">
          <div class="temperature-monthTitle">Januar</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 80px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Februar</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 110px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Marts</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 115px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">April</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 50px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Maj</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Juni</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Juli</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">August</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">September</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Oktober</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">November</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">December</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
      </div>
    </div>

    <div class="temperature-widgetWrap">
      <h5 class="temperature-widgetTitle">Vandtemperatur</h5>
      <div class="temperature-widget temperature-widget--half">
        <div class="temperature-month">
          <div class="temperature-monthTitle">Januar</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 80px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Februar</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 110px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Marts</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 115px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">April</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 50px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Maj</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Juni</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Juli</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">August</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">September</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Oktober</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">November</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">December</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">30°</div>
        </div>
      </div>
    </div>

    <div class="temperature-widgetWrap">
      <h5 class="temperature-widgetTitle">Nedbør (mm)</h5>
      <div class="temperature-widget temperature-widget--striped color-variant-1--1">
        <div class="temperature-month">
          <div class="temperature-monthTitle">Januar</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 80px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">92</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Februar</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 50px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">22</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Marts</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 65px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">58</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">April</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 85px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">108</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Maj</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner" style="width: 115px;">
            </div>
          </div>
          <div class="temperature-monthTemperature">224</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Juni</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">162</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Juli</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">145</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">August</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">189</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">September</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">208</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">Oktober</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">228</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">November</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">230</div>
        </div>
        <div class="temperature-month">
          <div class="temperature-monthTitle">December</div>
          <div class="temperature-monthVisual">
            <div class="temperature-monthVisualInner">
            </div>
          </div>
          <div class="temperature-monthTemperature">198</div>
        </div>
      </div>
    </div>
    
  </div>
</section> <!-- section 6 -->
<section class="section--wide section--hasTitle bg-variant--" id="section---">
  <div class="section-title">
    <div class="container--md text--center text--md--left">
      <strong>Fakta</strong> <br> Maldiverne
      <a href="#" class="float--right read-more">Fuld beskrivelse<span class="fa fa-angle-down"></span></a>
    </div>
  </div>
  <div class="container--md">
    <div class="accordion">
      <h3 class="accordion-header">
        Sanctuary Chief´s Camp
      </h3>
      <div class="accordion-content">
        <div class="text-widget grid mb--6">
          <div class="col--12 col--md--6">
            
            <img class="image--contain mb--1" src="assets/images/accordion-image.jpg">
            <p class="text--smaller">
              Serengeti Nationalpark
            </p>

          </div>
          <div class="col--12 col--md--6">
            <p>Navnet Serengeti kommer fra ”siringet”, der på Masai-folkets sprog betyder “endeløse sletter”, og med rette. Serengeti er et vildtområde på 14.763 km², hvor godt to tredjedele er sletter. I den nordligere del er landskabet meget kuperet, og i den centrale del er der tæt akacievegetation. </p>
            <p>I Serengeti finder man en bestand af storvildt, som ikke findes mage til andre steder på jorden. Mod nord grænser parken op til Masai Mara-reservatet i Kenya. De to parker danner et økosystem, hvor omkring to millioner dyr er på konstant vandring (”migration” på engelsk) efter nye græsgange – størstedelen er gnuer, zebraer og gazeller. Rovdyrene følger flokken som en skygge, altid på jagt efter næste måltid. </p>
            <p>I Serengeti er alle vildtarter repræsenteret, og safarien byder altid på spændende oplevelser. Perioden fra omkring december til juni er de bedste måneder at opleve migration.</p>
          </div>
        </div>
      </div> <!-- accordion-content -->
      <h3 class="accordion-header">
        Sanctuary Chief´s Camp
      </h3>
      <div class="accordion-content">
        <div class="text-widget grid mb--6">
          <div class="col--12 col--md--6">
            
            <img class="image--contain mb--1" src="assets/images/accordion-image.jpg">
            <p class="text--smaller">
              Serengeti Nationalpark
            </p>

          </div>
          <div class="col--12 col--md--6">
            <p>Navnet Serengeti kommer fra ”siringet”, der på Masai-folkets sprog betyder “endeløse sletter”, og med rette. Serengeti er et vildtområde på 14.763 km², hvor godt to tredjedele er sletter. I den nordligere del er landskabet meget kuperet, og i den centrale del er der tæt akacievegetation. </p>
            <p>I Serengeti finder man en bestand af storvildt, som ikke findes mage til andre steder på jorden. Mod nord grænser parken op til Masai Mara-reservatet i Kenya. De to parker danner et økosystem, hvor omkring to millioner dyr er på konstant vandring (”migration” på engelsk) efter nye græsgange – størstedelen er gnuer, zebraer og gazeller. Rovdyrene følger flokken som en skygge, altid på jagt efter næste måltid. </p>
            <p>I Serengeti er alle vildtarter repræsenteret, og safarien byder altid på spændende oplevelser. Perioden fra omkring december til juni er de bedste måneder at opleve migration.</p>
          </div>
        </div>
      </div> <!-- accordion-content -->

      <h3 class="accordion-header">
        Sanctuary Chief´s Camp
      </h3>
      <div class="accordion-content">
        <div class="text-widget grid mb--6">
          <div class="col--12 col--md--6">
            
            <img class="image--contain mb--1" src="assets/images/accordion-image.jpg">
            <p class="text--smaller">
              Serengeti Nationalpark
            </p>

          </div>
          <div class="col--12 col--md--6">
            <p>Navnet Serengeti kommer fra ”siringet”, der på Masai-folkets sprog betyder “endeløse sletter”, og med rette. Serengeti er et vildtområde på 14.763 km², hvor godt to tredjedele er sletter. I den nordligere del er landskabet meget kuperet, og i den centrale del er der tæt akacievegetation. </p>
            <p>I Serengeti finder man en bestand af storvildt, som ikke findes mage til andre steder på jorden. Mod nord grænser parken op til Masai Mara-reservatet i Kenya. De to parker danner et økosystem, hvor omkring to millioner dyr er på konstant vandring (”migration” på engelsk) efter nye græsgange – størstedelen er gnuer, zebraer og gazeller. Rovdyrene følger flokken som en skygge, altid på jagt efter næste måltid. </p>
            <p>I Serengeti er alle vildtarter repræsenteret, og safarien byder altid på spændende oplevelser. Perioden fra omkring december til juni er de bedste måneder at opleve migration.</p>
          </div>
        </div>
      </div> <!-- accordion-content -->

      <h3 class="accordion-header">
        Sanctuary Chief´s Camp
      </h3>
      <div class="accordion-content">
        <div class="text-widget grid mb--6">
          <div class="col--12 col--md--6">
            
            <img class="image--contain mb--1" src="assets/images/accordion-image.jpg">
            <p class="text--smaller">
              Serengeti Nationalpark
            </p>

          </div>
          <div class="col--12 col--md--6">
            <p>Navnet Serengeti kommer fra ”siringet”, der på Masai-folkets sprog betyder “endeløse sletter”, og med rette. Serengeti er et vildtområde på 14.763 km², hvor godt to tredjedele er sletter. I den nordligere del er landskabet meget kuperet, og i den centrale del er der tæt akacievegetation. </p>
            <p>I Serengeti finder man en bestand af storvildt, som ikke findes mage til andre steder på jorden. Mod nord grænser parken op til Masai Mara-reservatet i Kenya. De to parker danner et økosystem, hvor omkring to millioner dyr er på konstant vandring (”migration” på engelsk) efter nye græsgange – størstedelen er gnuer, zebraer og gazeller. Rovdyrene følger flokken som en skygge, altid på jagt efter næste måltid. </p>
            <p>I Serengeti er alle vildtarter repræsenteret, og safarien byder altid på spændende oplevelser. Perioden fra omkring december til juni er de bedste måneder at opleve migration.</p>
          </div>
        </div>
      </div> <!-- accordion-content -->

      <h3 class="accordion-header">
        Sanctuary Chief´s Camp
      </h3>
      <div class="accordion-content">
        <div class="text-widget grid mb--6">
          <div class="col--12 col--md--6">
            
            <img class="image--contain mb--1" src="assets/images/accordion-image.jpg">
            <p class="text--smaller">
              Serengeti Nationalpark
            </p>

          </div>
          <div class="col--12 col--md--6">
            <p>Navnet Serengeti kommer fra ”siringet”, der på Masai-folkets sprog betyder “endeløse sletter”, og med rette. Serengeti er et vildtområde på 14.763 km², hvor godt to tredjedele er sletter. I den nordligere del er landskabet meget kuperet, og i den centrale del er der tæt akacievegetation. </p>
            <p>I Serengeti finder man en bestand af storvildt, som ikke findes mage til andre steder på jorden. Mod nord grænser parken op til Masai Mara-reservatet i Kenya. De to parker danner et økosystem, hvor omkring to millioner dyr er på konstant vandring (”migration” på engelsk) efter nye græsgange – størstedelen er gnuer, zebraer og gazeller. Rovdyrene følger flokken som en skygge, altid på jagt efter næste måltid. </p>
            <p>I Serengeti er alle vildtarter repræsenteret, og safarien byder altid på spændende oplevelser. Perioden fra omkring december til juni er de bedste måneder at opleve migration.</p>
          </div>
        </div>
      </div> <!-- accordion-content -->

      <h3 class="accordion-header">
        Sanctuary Chief´s Camp
      </h3>
      <div class="accordion-content">
        <div class="text-widget grid mb--6">
          <div class="col--12 col--md--6">
            
            <img class="image--contain mb--1" src="assets/images/accordion-image.jpg">
            <p class="text--smaller">
              Serengeti Nationalpark
            </p>

          </div>
          <div class="col--12 col--md--6">
            <p>Navnet Serengeti kommer fra ”siringet”, der på Masai-folkets sprog betyder “endeløse sletter”, og med rette. Serengeti er et vildtområde på 14.763 km², hvor godt to tredjedele er sletter. I den nordligere del er landskabet meget kuperet, og i den centrale del er der tæt akacievegetation. </p>
            <p>I Serengeti finder man en bestand af storvildt, som ikke findes mage til andre steder på jorden. Mod nord grænser parken op til Masai Mara-reservatet i Kenya. De to parker danner et økosystem, hvor omkring to millioner dyr er på konstant vandring (”migration” på engelsk) efter nye græsgange – størstedelen er gnuer, zebraer og gazeller. Rovdyrene følger flokken som en skygge, altid på jagt efter næste måltid. </p>
            <p>I Serengeti er alle vildtarter repræsenteret, og safarien byder altid på spændende oplevelser. Perioden fra omkring december til juni er de bedste måneder at opleve migration.</p>
          </div>
        </div>
      </div> <!-- accordion-content -->
      
    </div>
  </div>
</section> <!-- section 3 -->

<section class="section--wide section--hasTitle bg-variant-1--1" id="section--">
  <div class="section-title">
    <div class="container--md text--center text--md--left">
      Atoller <br> maldiverne
    </div>
  </div>

  <div class="container--md">
    <div class="text-widget grid">
      <div class="col--12 col--md--7 mb--3 mb--md--0">

        <!-- Slider small -->
        <div class="slider slider--small mb--4">
          <div class="slider-inner js--slider">
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
            <div class="slider-item">
              <a href="assets/images/slider-image--small.jpg" data-lightbox="slider--2">
                <img class="image--contain" src="assets/images/slider-image--small.jpg">
              </a>
            </div>
          </div>
          <div class="slider-expand">Store fotos</div>
          <div class="slider-container">
            <div class="js--slider-actions slider-actions">
            </div>
          </div>
        </div>
        <!-- Slider small end. -->

      </div>
      <div class="col--12 col--md--5">
        <p class="text--leader">
          Den største attraktion på Maldiverne er uden tvivl de kridhvide koralstrande og det krystalklare vand med fisk i alle farver.
        </p>
        <p>
          Maldiverne gemmer på nogle af de bedste dykker- og snorkleoplevelser i verden med tropefisk i alverdens farver, havskildpadder og fantastiske koralformationer. Øgruppen er et eftertragtet sted for dykkere og windsurfere, både begyndere og erfarne, der søger det bedste inden for begge sportsgrene.
        </p>
        <p>
          Er man til dybhavsfiskeri hører Maldiverne også blandt de bedste steder i verden.
        </p>

        <div class="list list--bordered list--2columns">
          
          <div class="list-item">Snorkling og dykning</div>
          <div class="list-item">Dybhavsfiskeri</div>
          <div class="list-item">Vandsport</div>
          <div class="list-item">Vandreture</div>
          <div class="list-item">Strandture</div>
          <div class="list-item">Helseklub</div>
          <div class="list-item">Hyggelige restauranter</div>

        </div>


      </div>
    </div>
    
  </div>
</section> <!-- section -->
<section class="section--wide search--section bg-variant-1--1" id="section---">

  <div class="search-filters--mobile hidden--md--up">
    <div class="search-filterQuery">
      <i class="icon--search"></i>
      <input type="text" placeholder="What are you looking for?">
      <i class="js--search-filterTrigger icon--close"></i>
    </div>
    <div class="js--search-filterTrigger search-filterTrigger bg-darken--3">
      <span class="icon--sliders"></span>
      <span class="color--white">Filter results <small class="hidden--md--up">(182)</small></span>
      <span class="icon--arrowRightWhite icon--arrow"></span>
    </div>
    <div class="search-filtersWrap">
      <div class="search-filter toggle">
        <input type="checkbox" class="toggle-checkbox" id="mobile-filter-where">
        <label class="search-filterTitle toggle-label" for="mobile-filter-where">
          Where
          <span class="icon--arrowRightWhite icon--arrow"></span>
        </label>
        <div class="search-filterItems toggle-container">
          <div class="search-addFilterItem select">
            <select>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
          </div>
          <div class="search-filterItem">
            Maldiverne
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Mauritius
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Mozambique
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Seychellerne
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Sri Lanka
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Zanzibar
            <span class="search-filterItemRemove"></span>
          </div>
        </div>
      </div> <!-- search filter where -->
      <div class="search-filter toggle">
        <input type="checkbox" class="toggle-checkbox" id="mobile-filter-what">
        <label class="search-filterTitle toggle-label" for="mobile-filter-what">
          What
          <span class="icon--arrowRightWhite icon--arrow"></span>
        </label>
        <div class="search-filterItems toggle-container">
          <div class="search-addFilterItem select">
            <select>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
          </div>
          <div class="search-filterItem">
            Bryllupsrejser
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            All Inclusive rejser
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Vip All Inclusive
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Golfrejser
            <span class="search-filterItemRemove"></span>
          </div>
        </div>
      </div> <!-- search filter what -->
      <div class="search-filter toggle">
        <input type="checkbox" class="toggle-checkbox" id="mobile-filter-when">
        <label class="search-filterTitle toggle-label" for="mobile-filter-when">
          When
          <span class="icon--arrowRightWhite icon--arrow"></span>
        </label>
        <div class="search-filterItems toggle-container">
          <div class="search-addFilterItem select">
            <select>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
          </div>
          <div class="search-filterItem">
            Bryllupsrejser
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            All Inclusive rejser
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Vip All Inclusive
            <span class="search-filterItemRemove"></span>
          </div>
          <div class="search-filterItem">
            Golfrejser
            <span class="search-filterItemRemove"></span>
          </div>
        </div>
      </div> <!-- search filter when -->
      <div class="search-filter toggle">
        <input type="checkbox" class="toggle-checkbox" id="mobile-filter-price">
        <label class="search-filterTitle toggle-label" for="mobile-filter-price">
          Price
          <span class="icon--arrowRightWhite icon--arrow"></span>
        </label>
        <div class="search-filterItems search-filterItems--price toggle-container">
          <div class="js--search-priceSlider search-priceSlider">
            <div class="search-priceValues">
              <div class="search-price search-price--from">
                <span class="js--price-from">0.000</span>
                <span>DDK</span>
              </div>
              <div class="search-price search-price--to">
                <span class="js--price-to">0.000</span>
                <span>DDK</span>
              </div>
            </div>
            <div class="js--price-slider"></div>
          </div>
        </div>
      </div> <!-- search filter price-->
      <div class="search-filter toggle">
        <input type="checkbox" class="toggle-checkbox" id="mobile-filter-rating">
        <label class="search-filterTitle toggle-label" for="mobile-filter-rating">
          <form class="rate rate--white">
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--1">
            <label for="rate-star--1" class="rate-star"></label>
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--2">
            <label for="rate-star--2" class="rate-star"></label>
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--3">
            <label for="rate-star--3" class="rate-star"></label>
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--4" checked="checked">
            <label for="rate-star--4" class="rate-star"></label>
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--5">
            <label for="rate-star--5" class="rate-star"></label>
          </form>
          <span class="icon--arrowRightWhite icon--arrow"></span>
        </label>
        <div class="search-filterItems search-filterItems--rating toggle-container">
          <form class="rate rate--gold rate--active">
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--1">
            <label for="rate-star--1" class="rate-star"></label>
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--2">
            <label for="rate-star--2" class="rate-star"></label>
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--3">
            <label for="rate-star--3" class="rate-star"></label>
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--4" checked="checked">
            <label for="rate-star--4" class="rate-star"></label>
            <input type="radio" name="rate" class="rate-starCheckbox" id="rate-star--5">
            <label for="rate-star--5" class="rate-star"></label>
          </form>
        </div>
      </div> <!-- search filter rating-->
    </div>
    <div class="search-filterResults">
      <button class="button button--text">Show results (1449)</button>
    </div>
  </div>

  <div class="search-resultsCount bg-darken--2">
    Hoteller pa Maldiverne: <strong>35</strong> resultater
  </div>
  <div class="container--lg search-results">
    <div class="grid">
      <div class="col--12 col--md--4 mb--4 mb--md--0 hidden--sm--down">
        <form class="search-quick">
          <div class="select select--white w--100">
            <select name="where" id="">
              <option value="" disabled selected>Where?</option>
              <option value="">Option 1</option>
              <option value="">Option 2</option>
              <option value="">Option 3</option>
              <option value="">Option 4</option>
            </select>
          </div>
          <div class="select select--white w--100">
            <select name="what" id="">
              <option value="" disabled selected>What?</option>
              <option value="">Option 1</option>
              <option value="">Option 2</option>
              <option value="">Option 3</option>
              <option value="">Option 4</option>
            </select>
          </div>
          <div class="select-group d--flex">
            <div class="select select--white select--calendar w--50">
              <select name="where" id="">
                <option value="" disabled selected>Depart</option>
                <option value="">Option 1</option>
                <option value="">Option 2</option>
                <option value="">Option 3</option>
                <option value="">Option 4</option>
              </select>
            </div>
            <div class="select select--white select--calendar w--50">
              <select name="what" id="">
                <option value="" disabled selected>Return</option>
                <option value="">Option 1</option>
                <option value="">Option 2</option>
                <option value="">Option 3</option>
                <option value="">Option 4</option>
              </select>
            </div>
          </div> <!-- select group -->

          <div class="select select--white select--last w--100">
            <select name="what" id="">
              <option value="" disabled selected>Number of persons?</option>
              <option value="">Option 1</option>
              <option value="">Option 2</option>
              <option value="">Option 3</option>
              <option value="">Option 4</option>
            </select>
          </div>
          <button class="button button--main-2 button--block button--large">Søg</button>
        </form>
        <div class="search-filter bg-variant-1--3">
          <header class="search-filterHeader bg-variant-1--4">
            <span class="icon--sliders"></span>
            <span class="">Filter results</span>
            <a href="" class="search-clear">Clear all</a>
          </header>
          <div class="search-filterBody">
            <div class="search-section search-section--query">
              <div class="search-query d--flex">
                <input class="input input--text w--100" type="text" placeholder="Hotelname">
              </div>
            </div>
            <div class="search-section">
              <header class="search-sectionHeader">
                <span>Where</span>
                <a href="" class="search-clear">Clear</a>
              </header>
              <div class="search-sectionBody">
                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="text--uppercase" for="all--1">
                    <span class="checkbox-box bg-variant-1--5"></span>
                    Det Indiske Ocean
                  </label>
                </div>
                <div class="search-checkboxes-2columns">
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Maldiverne
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Mauritius
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Mozanbique
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Seychellerne
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Sri lanka
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Zanzibar
                    </label>
                  </div>
                </div> <!-- search-checkboxes-2columns -->

                <div class="spacer"></div>

                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="text--uppercase" for="all--1">
                    <span class="checkbox-box bg-variant-1--5"></span>
                    Afrika
                  </label>
                </div>
                <div class="search-checkboxes-2columns">
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Botsuana
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1" checked="checked">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Kenya
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Namibia
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Rwanda
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Sydafrika
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Tanzania
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Uganda
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Zambia
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Zimbambwe
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Zanzibar
                    </label>
                  </div>
                </div> <!-- search-checkboxes-2columns -->

                <div class="spacer"></div>

                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="text--uppercase" for="all--1">
                    <span class="checkbox-box bg-variant-1--5"></span>
                    Mellemosten
                  </label>
                </div>
                <div class="search-checkboxes-2columns">
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Abu Dhabi
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Dubai
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Oman
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Ras of Khaimanh
                    </label>
                  </div>
                </div> <!-- search-checkboxes-2columns -->

                <div class="spacer"></div>

                <i class="icon--mapRoute"></i>
                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="text--uppercase" for="all--1">
                    <span class="checkbox-box bg-variant-1--5"></span>
                    Kombinationersrejser
                  </label>
                </div>

              </div> <!-- search-sectionBody -->
            </div> <!-- search-section -->
            <div class="search-section">
              <header class="search-sectionHeader">
                <span>What</span>
                <a href="" class="search-clear">Clear</a>
              </header>
              <div class="search-sectionBody">
                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="text--uppercase" for="all--1">
                    <span class="checkbox-box bg-variant-1--5"></span>
                    Alle Rejsertype
                  </label>
                </div>
                <div class="search-checkboxes-2columns">
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Brullupsrejser
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      All Inclusive
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      VIP All Inclusive
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Golfrejser
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Eksotiska vejser
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Safarirejser til Afrika
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Rejser med born
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Adventure rejser
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Togrejser
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Kombinationsrejser
                    </label>
                  </div>
                  <div class="checkbox">
                    <input type="checkbox" name="all-1" id="all--1">
                    <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Luksusrejser
                    </label>
                  </div>
                </div> <!-- search-checkboxes-2columns -->

              </div> <!-- search-sectionBody -->
            </div> <!-- search-section -->
            <div class="search-section">
              <header class="search-sectionHeader">
                <span>Kriterier</span>
                <a href="" class="search-clear">Clear</a>
              </header>
              <div class="search-sectionBody">
                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Privat pool
                    </label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Privat villa
                    </label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Velegnet for pat
                    </label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      All inclusive
                    </label>
                </div>
                <div class="checkbox">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span>
                      Egnet for famllier
                    </label>
                </div>
              </div> <!-- search-sectionBody -->
            </div> <!-- search-section -->
            <div class="search-section">
              <header class="search-sectionHeader">
                <span>Price</span>
              </header>
              <div class="search-sectionBody">
                <div class="js--search-priceSlider search-priceSlider">
                  <div class="search-priceValues">
                    <div class="search-price search-price--from">
                      <span class="js--price-from">0.000</span>
                      <span>DDK</span>
                    </div>
                    <div class="search-price search-price--to">
                      <span class="js--price-to">0.000</span>
                      <span>DDK</span>
                    </div>
                  </div>
                  <div class="js--price-slider"></div>
                </div>
              </div>
            </div> <!-- search-section -->
            <div class="search-section search-section--noBorder">
              <header class="search-sectionHeader">
                <span>Standart</span>
              </header>
              <div class="search-sectionBody">
                <div class="checkbox checkbox--rate">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span
                        >
                      
                    <i class="icon--starWhite"></i>
                    <i class="icon--starWhite"></i>
                    <i class="icon--starWhite"></i>
                    <span class="search-rateResultsCount">(14)</span>
                  </label>
                </div>
                <div class="checkbox checkbox--rate">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span
                        >
                      
                    <i class="icon--starWhite"></i>
                    <i class="icon--starWhite"></i>
                    <i class="icon--starWhite"></i>
                    <i class="icon--starWhite"></i>
                    <span class="search-rateResultsCount">(6)</span>
                  </label>
                </div>
                <div class="checkbox checkbox--rate">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span
                        >
                      
                    <i class="icon--starWhite"></i>
                    <i class="icon--starWhite"></i>
                    <i class="icon--starWhite"></i>
                    <i class="icon--starWhite"></i>
                    <i class="icon--starWhite"></i>
                    <span class="search-rateResultsCount">(24)</span>
                  </label>
                </div>
                <div class="checkbox checkbox--rate">
                  <input type="checkbox" name="all-1" id="all--1">
                  <label class="" for="all--1">
                      <span class="checkbox-box bg-variant-1--5"></span
                        >
                      
                    <i class="icon--starWhite"></i>
                    <i class="icon--starWhite"></i>
                    <i class="icon--starWhite"></i>
                    <i class="icon--starWhite"></i>
                    <i class="icon--starWhite"></i>
                    <i class="icon--starWhite"></i>
                    <span class="search-rateResultsCount">(9)</span>
                  </label>
                </div>
              </div>
            </div> <!-- search-section -->
            <div class="search-section search-section--noBorder">
              <div class="search-sectionBody">
                <button class="button button--text button--resultsFound">Results found (1449)</button>
              </div>
            </div> <!-- search-section -->
          </div>
        </div>
      </div> <!-- search-wrap -->
      <div class="col--12 col--md--8">
        <div class="search-order">
          <div class="select">
            <select name="" id="">
              <option value="">Our fauvourites</option>
            </select>
          </div>
          <div class="select">
            <select name="" id="">
              <option value="">Star - High</option>
            </select>
          </div>
          <div class="select">
            <select name="" id="">
              <option value="">Price -High</option>
            </select>
          </div>

          <a href="" class="search-linkMap">
            <i class="icon--map"></i>
            <span>Vis på kort</span>
          </a>

        </div>
        <div class="grid tour-list tour-list--search">
          <?php for($i=0;$i<15;$i++):?>
            <div class="col--12 col--md--6">
              <?php include('partials/tour-list-item.php'); ?>
            </div>
          <?php endfor; ?>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section--blockquote blockquote-widget" id="section--8" style="background: url(assets/images/blockquote-example.jpg) center/cover no-repeat">
  <blockquote class="blockquote-widgetContainer" style="background: white">
    <div class="js--blockquote-carousel blockquote-carousel">
      <div class="blockquote-carouselItem">
        <cite class="blockquote-cite">
          “Maldiverne er det sidste paradis på jorden. Ønsker man en uspoleret tropisk ø med palmer svajende i tropebrisen, kridhvide sandstrande og fantastiske turkisgrønne laguner – så bliver man ikke skuffet.”
        </cite>
        <div class="blockquote-author">Lonely planet</div>
      </div>
      <div class="blockquote-carouselItem">
        <cite class="blockquote-cite">
          “skuffet er det sidste paradis på jorden. Ønsker man en uspoleret tropisk ø med palmer svajende i tropebrisen, kridhvide sandstrande og fantastiske turkisgrønne laguner – så bliver man ikke Maldiverne.”
        </cite>
        <div class="blockquote-author">Lonely planet</div>
      </div>
    </div>
    <a href="" class="button button--variant--1 blockquote-button">læs mere</a>
  </blockquote>
</section> <!-- section 8-->
<section class="section--wide py--0" id="section--9">
  <!-- Slider big -->
  <div class="slider slider--medium mh--md--50vh">
    <div class="slider-inner js--slider slider--dots">
      <div class="slider-item" style="background-image:url('assets/images/slider/anantara-kihavah-first-class-holiday.jpg')"></div>
      <div class="slider-item" style="background-image:url('assets/images/slider/anantara-kihavah-maldives-island-resort.jpg')"></div>
      <div class="slider-item" style="background-image:url('assets/images/slider/anantara-kihavah-maldives-overwater-spa.jpg')"></div>
      <div class="slider-item" style="background-image:url('assets/images/slider/anantara-kihavah-maldives-villa-walkway.jpg')"></div>
    </div>
    <div class="slider-container">
      <div class="container--xl">
        <h3>Anantara
          <br>
          Kihavah Villas
        </h3>
        <div class="includes">
          <img src="assets/images/1x/icon--couple.png"/>
          <img src="assets/images/1x/icon--family.png"/>
          <img src="assets/images/1x/icon--island.png"/>
          <img src="assets/images/1x/icon--scubadiving.png"/>
        </div>
        <div class="js--slider-actions slider-actions"></div>
      </div>
    </div>
  </div>
  <!-- Slider big end. -->
</section> <!-- section 9 -->