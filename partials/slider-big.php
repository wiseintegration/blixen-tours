<div class="slider-wrap">
  <div class="slider slider-big js--slider">
    <div class="slider-item" style="background-image:url('assets/images/slider-image.jpg')">
    </div>
    <div class="slider-item" style="background-image:url('assets/images/slider-image.jpg')">
    </div>
    <div class="slider-item" style="background-image:url('assets/images/slider-image.jpg')">
    </div>
  </div>
  <div class="slider-container">
    <div class="container--xl py--8">
      <p class="text--meta mt--5 mb--5">Destinationer / Det indiske ocean</p>
      <h3 class="color--main font--italic mb--4">Rejser til 
        <br>
        Maldiverne
      </h3>
      <img class="mb--8" src="assets/images/slider-graphic.png" alt="">
      <div class="js--slider-actions slider-actions"></div>
    </div>
  </div>
</div>