
<!DOCTYPE html>
<html lang="en">

<?php include_once('head.php'); ?>

<body class="page">
  <div class="page-wrap">
    <header class="header--global">
      <div class="header-inner container--xl">
        <a href="#" class="nav-toggle js--nav-toggle"><span></span></a>
        <a href="#" class="logo logo--header text--hide">Blixen Tours</a>

        <?php include_once('nav-global.php'); ?>
        <?php include_once('header-aside.php'); ?>

      </div>
    </header>
