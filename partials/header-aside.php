<aside class="header-aside">
  <div class="contacts hidden--sm--down">
    <span class="contacts-icon--phone"></span>
    <div class="contacts-info">
      <a href="tel:+4576740070" class="contacts-phone">
        +45 7674 0070
      </a>
      <div class="contacts-workHours toggle">
        <input class="checkbox--toggle toggle-checkbox" type="checkbox" id="workHoursToggle">
        <label class="contacts-workHoursToggle toggle-label" for="workHoursToggle">
          Åben i dag 09-20 <i class="contacts-icon--arrowDown"></i>
        </label>
        <div class="contacts-workHoursDropdown toggle-container">
          <div>Åben i dag 09-20</div>
          <div>Åben i dag 09-20</div>
          <div>Åben i dag 09-20</div>
        </div>
      </div>
    </div>
  </div> <!-- contacts -->
  <div class="search search--header">
    <form class="search-form" action="">
      <input type="text" class="search-input js--search-input">
      <button class="js--search-submit search-submit"><i class="search-icon"></i></button>
    </form>
    <div class="search-results">
      <div class="search-resultsRow">
        <div class="search-resultsCategory">
          Rejser
        </div>
        <div class="search-resultsItems">
          <a href="#" class="search-resultsItem">
            Kenya
          </a>
          <a href="#" class="search-resultsItem">
            Safari i Kenya
          </a>
          <a href="#" class="search-resultsItem">
            Rejser til Kenya
          </a>
          <a href="#" class="search-resultsItem">
            Safari i Kenya
          </a>
        </div>
      </div>
      <div class="search-resultsRow">
        <div class="search-resultsCategory">
          Artikler
        </div>
        <div class="search-resultsItems">
          <a href="#" class="search-resultsItem">
            Safari i Kenya
          </a>
        </div>
      </div>
    </div>
  </div>
</aside>